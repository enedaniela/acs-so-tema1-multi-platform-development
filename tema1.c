//Daniela Ene
//332CC

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "hash.h"


#define DIE(assertion, call_description)				\
	do {								\
		if (assertion) {					\
			fprintf(stderr, "(%s, %d): ",			\
					__FILE__, __LINE__);		\
			perror(call_description);			\
			exit(EXIT_FAILURE);				\
		}							\
} while (0)

int hashSize;
char ***hashMap;
int *bucket_sizes;
int needs_free = 1;

void add_word(char *word)
{
	int i = 0;
	int found = 0;
	int bucket = hash(word, hashSize);

	//verific daca cuvantul exista
	for (i = 0; i < bucket_sizes[bucket]; i++) {
		if (strcmp(hashMap[bucket][i], word) == 0)
			found = 1;
	}
	if (!found) {
		//gasesc bucket-ul
		bucket = hash(word, hashSize);
		//daca este primul cuvant din bucket, apelez malloc
		if (bucket_sizes[bucket] == 0) {
			bucket_sizes[bucket]++;
			hashMap[bucket] = malloc(sizeof(char *));
		} else{
			//daca mai exista cuvinte in bucket, apelez realloc
			bucket_sizes[bucket]++;
			hashMap[bucket] = realloc(hashMap[bucket],
				sizeof(char *) * bucket_sizes[bucket]);
		}
		//copiez cuvantul
		hashMap[bucket][bucket_sizes[bucket] - 1] = strdup(word);
		needs_free = 1;
	}
}

void remove_word(char *word)
{
	int i = 0;
	int j = 0;
	int found = 0;
	int bucket = hash(word, hashSize);

	//verific daca cuvantul exista
	for (i = 0; i < bucket_sizes[bucket]; i++) {
		if (strcmp(hashMap[bucket][i], word) == 0)
			found = 1;
	}
	if (found) {
		for (i = 0; i < bucket_sizes[bucket]; i++) {
			if (strcmp(hashMap[bucket][i], word) == 0) {
				free(hashMap[bucket][i]);
				for (j = i; j < bucket_sizes[bucket] - 1; j++)
					//shiftez toti pointerii
					hashMap[bucket][j] =
					hashMap[bucket][j + 1];
				//scad dimensiunea bucket-ului
				bucket_sizes[bucket]--;
				break;
			}
		}
		//realoc dimensiunea bucket-ului
		hashMap[bucket] = realloc(hashMap[bucket], sizeof(char *)
			* bucket_sizes[bucket]);
	}
}

void find(char *word, char *file)
{
	int i = 0;
	int found = 0;
	FILE *f;
	int bucket = hash(word, hashSize);

	//caut cuvantul
	for (i = 0; i < bucket_sizes[bucket]; i++) {
		if (strcmp(hashMap[bucket][i], word) == 0)
			found = 1;
	}
	//scriu fie la stdin, fie in fisier
	if (strcmp(file, "stdin") == 0) {
		if (found == 1)
			printf("True\n");
		else
			printf("False\n");
	} else{
		f = fopen(file, "a");
		if (found == 1)
			fprintf(f, "True\n");
		else
			fprintf(f, "False\n");
		fclose(f);
	}
}

void clear(void)
{
	int i = 0;
	int j = 0;

	//free la elementele din fiecare bucket
	for (i = 0; i < hashSize; i++)
		for (j = 0; j < bucket_sizes[i]; j++)
			free(hashMap[i][j]);
	//free la toate bucket-urile
	for (i = 0; i < hashSize; i++)
		if (bucket_sizes[i] != 0)
			free(hashMap[i]);
	//reinitializez vectorul de dimensiuni
	for (i = 0; i < hashSize; i++)
		bucket_sizes[i] = 0;
	needs_free = 0;
}

void print_bucket(int index_bucket, char *out)
{
	int j = 0;
	FILE *f;

	if (index_bucket < hashSize) {
		if (bucket_sizes[index_bucket] != 0) {
			for (j = 0; j < bucket_sizes[index_bucket]; j++) {
				if (strcmp(out, "stdin") == 0) {
					printf("%s ", hashMap[index_bucket][j]);
				} else{
					f = fopen(out, "a");
					fprintf(f, "%s ",
						hashMap[index_bucket][j]);
					fclose(f);
				}
			}
			if (strcmp(out, "stdin") == 0) {
				printf("\n");
			} else{
				f = fopen(out, "a");
				fprintf(f, "\n");
				fclose(f);
			}
		}
	}

}

void print_hash(char *out)
{
	int i, j;
	FILE *f;

	if (strcmp(out, "stdin") == 0) {
		for (i = 0; i < hashSize; i++) {
			for (j = 0; j < bucket_sizes[i]; j++)
				printf("%s ", hashMap[i][j]);
			if (bucket_sizes[i] > 0)
				printf("\n");
		}
	} else{
		for (i = 0; i < hashSize; i++) {
			if (bucket_sizes[i] != 0) {
				for (j = 0; j < bucket_sizes[i]; j++) {
					f = fopen(out, "a");
					if (j == bucket_sizes[i] - 1)
						fprintf(f, "%s",
							hashMap[i][j]);
					else
						fprintf(f, "%s ",
							hashMap[i][j]);
					fclose(f);
				}
				f = fopen(out, "a");
				fprintf(f, "\n");
				fclose(f);
			}
		}
	}
}

void resize_double(void)
{
	int i = 0;
	int j = 0;
	int bucket;
	int new_dim = 2 * hashSize;
	int *new_bucket_sizes = malloc(sizeof(int) * new_dim);
	//aloc noul hash
	char ***new_hash = malloc(sizeof(char **) * new_dim);

	for (i = 0; i < new_dim; i++)
		new_bucket_sizes[i] = 0;

	for (i = 0; i < hashSize; i++) {
		for (j = 0; j < bucket_sizes[i]; j++) {
			//calculez noul bucket
			bucket = hash(hashMap[i][j], new_dim);
			//adaug cuvantul
			if (new_bucket_sizes[bucket] == 0) {
				new_bucket_sizes[bucket]++;
				new_hash[bucket] = malloc(sizeof(char *));
			} else{
				new_bucket_sizes[bucket]++;
				new_hash[bucket] = realloc(new_hash[bucket],
					sizeof(char *)
					* new_bucket_sizes[bucket]);
			}
			new_hash[bucket][new_bucket_sizes[bucket] - 1] =
				strdup(hashMap[i][j]);
		}
	}
	//fac clear la vechiul hash
	clear();
	//vechiul hash o sa pointeze catre noul hash
	hashMap = new_hash;
	free(bucket_sizes);
	bucket_sizes = new_bucket_sizes;
	hashSize = new_dim;

}

void resize_halve(void)
{
	int i = 0;
	int j = 0;
	int bucket = 0;
	int new_dim = hashSize/2;
	int *new_bucket_sizes = malloc(sizeof(int) * new_dim);
	//aloc noul hash
	char ***new_hash = malloc(sizeof(char **) * new_dim);

	for (i = 0; i < new_dim; i++)
		new_bucket_sizes[i] = 0;

	for (i = 0; i < hashSize; i++) {
		for (j = 0; j < bucket_sizes[i]; j++) {
			//calculez noul bucket
			bucket = hash(hashMap[i][j], new_dim);
			//adaug cuvantul
			if (new_bucket_sizes[bucket] == 0) {
				new_bucket_sizes[bucket]++;
				new_hash[bucket] = malloc(sizeof(char *));
			} else{
				new_bucket_sizes[bucket]++;
				new_hash[bucket] = realloc(new_hash[bucket],
					sizeof(char *)
					* new_bucket_sizes[bucket]);
			}
			new_hash[bucket][new_bucket_sizes[bucket] - 1] =
			strdup(hashMap[i][j]);
		}
	}
	//fac clear la vechiul hash
	clear();
	//vechiul hash o sa pointeze catre noul hash
	hashMap = new_hash;
	free(bucket_sizes);
	bucket_sizes = new_bucket_sizes;
	hashSize = new_dim;

}

//verific daca comanda este valida
int check_command(char *command)
{
	char *ch;
	char *array[3];
	int i = 0;
	int some_ret = 0;

	ch = strtok(command, " ");

	while (ch != NULL) {
		array[i++] = ch;
		ch = strtok(NULL, "\n ");
	}

	if (strcmp(array[0], "add") == 0)
		some_ret = 1;
	if (strcmp(array[0], "remove") == 0)
		some_ret = 1;
	if (strcmp(array[0], "find") == 0)
		some_ret = 1;
	if (strcmp(array[0], "clear\n") == 0)
		some_ret = 1;
	if (strcmp(array[0], "print_bucket") == 0)
		some_ret = 1;
	if (strcmp(array[0], "print") == 0 ||
		strcmp(array[0], "print\n") == 0)
		some_ret = 1;
	if (strcmp(array[0], "resize") == 0) {

		if (strcmp(array[1], "double") == 0)
			some_ret = 1;
		if (strcmp(array[1], "halve") == 0)
			some_ret = 1;
	}
	return some_ret;
}

//procesez comenzile
void process_input(char *command)
{
	char *array[3];
	int i = 0;
	{
		char *ch = strtok(command, " ");

		while (ch != NULL) {
			array[i++] = ch;
			ch = strtok(NULL, "\n ");
		}
	}
	//procesez add
	if (strcmp(array[0], "add") == 0)
		add_word(array[1]);
	//procesez remove
	if (strcmp(array[0], "remove") == 0)
		remove_word(array[1]);
	//procesez find
	if (strcmp(array[0], "find") == 0) {
		char *console = "stdin";

		if (i == 2)
			find(array[1], console);
		if (i == 3)
			find(array[1], array[2]);
	}
	//procesez clear
	if (strcmp(array[0], "clear\n") == 0)
		clear();
	//prcesez print_bucket
	if (strcmp(array[0], "print_bucket") == 0) {
		char *console = "stdin";
		int j = 0;
		int index_bucket;
		int length;

		//verific daca indexul bucket-ului este un numar
		length = strlen(array[1]);
		for (j = 0; j < length; j++)
			if (!isdigit(array[1][j])) {
				DIE(!isdigit(array[1][j]),
					"Entered input is not a number\n");
				exit(1);
			}

		index_bucket = atoi(array[1]);
		if (i == 2)
			print_bucket(index_bucket, console);
		if (i == 3)
			print_bucket(index_bucket, array[2]);
	}
	//procesez print
	if (strcmp(array[0], "print") == 0 ||
		strcmp(array[0], "print\n") == 0) {
		char *console = "stdin";

		if (i == 1)
			print_hash(console);
		if (i == 2)
			print_hash(array[1]);
	}
	//procesez resize
	if (strcmp(array[0], "resize") == 0) {

		if (strcmp(array[1], "double") == 0)
			resize_double();
		if (strcmp(array[1], "halve") == 0)
			resize_halve();
	}
}
int main(int argc, char *argv[])
{
	int i = 0, k = 0, j = 0;
	FILE *fsrc;
	char line[20000], to_check[20000];
	int nr_files;

	nr_files = argc - 2;
	//verific numarul de parametrii
	if (nr_files < 0) {
		DIE(nr_files < 0, "invalid number of param");
	} else{

	hashSize = atoi(argv[1]);
	if (hashSize > 0) {
	//aloc hash-ul
	hashMap = malloc(sizeof(char **) * hashSize);

	//aloc vectorul cu dimensiunile bucket-urile
	bucket_sizes = malloc(sizeof(int) * hashSize);

	//initializez vectorul de dimensiuni ale bucket-urilor
	for (k = 0; k < hashSize; k++)
		bucket_sizes[k] = 0;

	if (nr_files == 0) {
		//citesc de la stdin
		while (fgets(line, 20000, stdin) != NULL) {
			strcpy(to_check, line);
			if (check_command(to_check) ||
				!strcmp(to_check, "\n"))
				process_input(line);
			else
				break;
		}
		//citesc din fisiere
	} else if (nr_files > 0) {
		for (i = 2; i < argc; i++) {

			fsrc = fopen(argv[i], "r");

			if (fsrc == NULL) {
				printf("Error opening file");
			} else {
				while (fgets(line, 20000, fsrc) != NULL) {
					strcpy(to_check, line);
					if (check_command(to_check) ||
						!strcmp(to_check, "\n"))
						process_input(line);
					else
						break;
				}
			}
			fclose(fsrc);
		}
	}
	if (needs_free) {
		//free la elementele din fiecare bucket
		for (i = 0; i < hashSize; i++)
			for (j = 0; j < bucket_sizes[i]; j++)
				free(hashMap[i][j]);
		//free la toate bucket-urile
		for (i = 0; i < hashSize; i++)
			if (bucket_sizes[i] != 0)
				free(hashMap[i]);
		}
		//free la hashmap
		free(hashMap);
		free(bucket_sizes);
	} else
	DIE(hashSize == 0, "invalid hash size");
}
return 0;
}
