CC=cl
CFLAGS = /nologo /W3
build: tema1.exe

tema1.exe: tema1.obj hash.lib
	link /LIBPATH:. /nologo /out:$@ $**

tema1.obj: tema1.c
	$(CC) $(CFLAGS) /c $**
	
clean:
	del /Q /F tema1.exe tema1.ob